import 'dart:async';

import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:math_quiz_game/question-list.dart';
import 'package:percent_indicator/percent_indicator.dart';

void main() {
  runApp(MathQuiz());
}

class MathQuiz extends StatefulWidget {
  @override
  State<MathQuiz> createState() => MyApp();
}


class MyApp extends State<MathQuiz> {
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
        tools: const [
          DeviceSection(),
        ],
        builder: (context) => MaterialApp(
          debugShowCheckedModeBanner: false,
          useInheritedMediaQuery: true,
          builder: DevicePreview.appBuilder,
          locale: DevicePreview.locale(context),
          title: 'Math Quiz Game',
          home: const HomePage(),
        )
    );
  }
}


class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.pink.shade50,
      appBar: AppBar(
        title: const Text(
          'Math Quiz', style: TextStyle(
          fontWeight: FontWeight.w500,
          fontSize: 30.0,
        ),
        ),
        backgroundColor: Colors.pink.shade100,
      ),
      body: SingleChildScrollView(
          child:Container(
            alignment: Alignment.center,
            child: Column(
              children: [
                const SizedBox(
                  height: 50,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      alignment: FractionalOffset.center,
                      height: 50,
                      width: 50,
                      decoration: const BoxDecoration(
                          color: Colors.orange
                      ),
                      child: Text(
                        'M',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 30.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Container(
                      alignment: FractionalOffset.center,
                      height: 50,
                      width: 50,
                      decoration: const BoxDecoration(
                          color: Colors.pinkAccent
                      ),
                      child: Text(
                        'A', style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 30.0,
                        color: Colors.white,
                      ),
                      ),
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Container(
                      alignment: FractionalOffset.center,
                      height: 50,
                      width: 50,
                      decoration: const BoxDecoration(
                          color: Colors.purple
                      ),
                      child: Text(
                        'T', style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 30.0,
                        color: Colors.white,
                      ),
                      ),
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Container(
                      alignment: FractionalOffset.center,
                      height: 50,
                      width: 50,
                      decoration: const BoxDecoration(
                          color: Colors.green
                      ),
                      child: Text(
                        'H', style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 30.0,
                        color: Colors.white,
                      ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 15,
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      alignment: FractionalOffset.center,
                      height: 50,
                      width: 50,
                      decoration: const BoxDecoration(
                          color: Colors.pinkAccent
                      ),
                      child: Text(
                        'G',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 30.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Container(
                      alignment: FractionalOffset.center,
                      height: 50,
                      width: 50,
                      decoration: const BoxDecoration(
                          color: Colors.purple
                      ),
                      child: Text(
                        'A', style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 30.0,
                        color: Colors.white,
                      ),
                      ),
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Container(
                      alignment: FractionalOffset.center,
                      height: 50,
                      width: 50,
                      decoration: const BoxDecoration(
                          color: Colors.lightBlueAccent
                      ),
                      child: Text(
                        'M', style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 30.0,
                        color: Colors.white,
                      ),
                      ),
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Container(
                      alignment: FractionalOffset.center,
                      height: 50,
                      width: 50,
                      decoration: const BoxDecoration(
                          color: Colors.green
                      ),
                      child: Text(
                        'E', style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 30.0,
                        color: Colors.white,
                      ),
                      ),
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Container(
                      alignment: FractionalOffset.center,
                      height: 50,
                      width: 50,
                      decoration: const BoxDecoration(
                          color: Colors.orange
                      ),
                      child: Text(
                        'S', style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 30.0,
                        color: Colors.white,
                      ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 120,
                ),
                Container(
                  child: ElevatedButton(
                    style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.purple)),
                    child: const Text('START', style: TextStyle(fontSize: 60.0,
                    ),
                    ),
                    onPressed: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => QuizPage()),
                      );
                    },
                  ),
                ),
                const SizedBox(
                  height: 120,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: const Image(image: NetworkImage('imgs/kuromi.png'),width: 200,height: 200,),
                    ),
                  ],
                ),
              ],
            ),
          )
      ),
    );
  }
}

class QuizPage extends StatefulWidget {
  @override
  State<QuizPage> createState() => Quiz();
}



class Quiz extends State<QuizPage> {
  late Timer _timer;
  double value = 0;
  var score = 0;

  void startTimer() {
    const speed = Duration(milliseconds: 20);
    _timer = Timer.periodic(speed, (timer) {
      setState(() {
        if (value > 0) {
          setState(() {
            value > 0.005 ? value -= 0.005 : value = 0;
          });
        } else {
          setState(() {
            MessageDialog();
            timer.cancel();
            //TimeoutMessageDialog();
          });
        }
      });
    });
  }

  Future<void> MessageDialog() {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25)
            ),
            alignment: Alignment.center,
            backgroundColor: Colors.white,
            title: const Text('Game Over \n Try Again',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            ),
            content: Text('Score : $score',
              textAlign: TextAlign.center,
              style: const TextStyle(fontSize: 28, fontWeight: FontWeight.w300),
            ),
            actions: [
              TextButton(onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const HomePage()),
                );
              }, child: const Text('Exit',
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: Colors.redAccent),
              )),

            ],
          );
        });
  }


  void checkAns(int index) {
    var res ='';
    setState(() {
      if (question.optionList[index] == question.correctAns) {
        res = 'Correct';
        score++;
        question.generateQuestion();
        value = 1;
      } else if(question.optionList[index] != question.correctAns) {
        MessageDialog();
        value=0;
      }
    });
  }



  Question question = Question();
  @override
  void initState() {
    super.initState();
    question.generateQuestion();
    startTimer();
    value =1;

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.pink.shade50,
      appBar: AppBar(
        backgroundColor: Colors.pink.shade100,
        title: const Text(
          'Math', style: TextStyle(
          fontWeight: FontWeight.w500,
          fontSize: 30.0,),
        ),
      ),
      body: SingleChildScrollView(
          child:Container(
            alignment: Alignment.center,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, right: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children:  [
                      Text('SCORE: $score', style: const TextStyle(fontSize: 20, color: Colors.black),)
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        Container(
                          height: 200,
                          width: 200,
                          margin: const EdgeInsets.fromLTRB(50, 50, 50, 50),
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.circle
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 45.0),
                          child: CircularPercentIndicator(
                            radius: 105.0,
                            lineWidth: 10,
                            percent: value,

                            progressColor: value > 0.6 ? Colors.green : value > 0.3 ? Colors.yellow : Colors.red,
                          ),
                        ),
                        Padding(padding: const EdgeInsets.only(top: 130.0),
                          child: Text(question.question,style: const TextStyle(fontSize: 36),
                          ),
                        ),
                        //  Padding(padding:  EdgeInsets.only(top: 200.0),
                        //   child:  Text('',style:  TextStyle(fontSize: 36),
                        //   ),
                        //),
                      ],
                    )
                  ],
                ),
                Row(

                  mainAxisAlignment: MainAxisAlignment.center,
                  children:[
                    ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(Colors.lightBlue.shade100),
                            padding: MaterialStateProperty.all(const EdgeInsets.all(60)),
                            textStyle: MaterialStateProperty.all(
                                const TextStyle(fontSize: 14,))),
                        onPressed:() {
                          setState(() {
                            checkAns(1);
                          });
                        },
                        child: Text(question.optionList[1].toString(),style: const TextStyle(color: Colors.black,fontSize: 30),)),

                    const SizedBox(
                      width: 50,
                    ),
                    ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(Colors.yellow.shade100),
                            padding:
                            MaterialStateProperty.all(const EdgeInsets.all(60)),
                            textStyle: MaterialStateProperty.all(
                                const TextStyle(fontSize: 14,))),
                        onPressed:() {
                          setState(() {
                            checkAns(2);
                          });
                        },
                        child: Text(question.optionList[2].toString(),style: const TextStyle(color: Colors.black,fontSize: 30),)),
                  ],
                ),
                const SizedBox(
                  height: 50,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children:[

                    ElevatedButton(
                        style: ButtonStyle(

                            backgroundColor: MaterialStateProperty.all(Colors.purple.shade100),
                            padding: MaterialStateProperty.all(const EdgeInsets.all(60)),
                            textStyle: MaterialStateProperty.all(
                                const TextStyle(fontSize: 14,))),
                        onPressed:() {
                          setState(() {
                            checkAns(3);
                          });
                        },
                        child: Text(question.optionList[3].toString(),style: const TextStyle(color: Colors.black,fontSize: 30),)),
                    const SizedBox(
                      width: 50,
                    ),
                    ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(Colors.green.shade100),
                            padding:
                            MaterialStateProperty.all(const EdgeInsets.all(60)),
                            textStyle: MaterialStateProperty.all(
                                const TextStyle(fontSize: 14,))),
                        onPressed:() {
                          setState(() {
                            checkAns(4);
                          });
                        },
                        child: Text(question.optionList[4].toString(),style: const TextStyle(color: Colors.black,fontSize: 30),)),
                  ],
                ),
              ],
            ),
          )
      ),
    );
  }
}
