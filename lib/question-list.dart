import 'dart:math';


class Question {
  String question = '';
  List<int> optionList = [];
  int score = 0;
  int correctAns = 0;
  String res = '';

  void generateQuestion() {
    Random random = Random();
    int first = random.nextInt(10);
    int second = random.nextInt(10);
    question = '$first + $second';
    correctAns = first + second;
    int correctAnsLocation = random.nextInt(4);
    int incorrectAns;

    //first clear optionList
    optionList.clear();
    for (int i = 0; i < 4; i++) {
      if (i == correctAnsLocation) optionList.add(correctAns);

      incorrectAns = random.nextInt(20);
      while (correctAns == incorrectAns) {
        incorrectAns = random.nextInt(20);
      }

      optionList.add(incorrectAns);
    }
  }
}